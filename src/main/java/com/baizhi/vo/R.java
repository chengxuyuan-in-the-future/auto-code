package com.baizhi.vo;

import lombok.Data;

@Data
public class R<T> {
    private int code;
    private String message;
    private T data;
    private R(int code, String message, T data){
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public static Builder builder(){
        return new Builder();
    }

    public static Builder code(int code){
        return new Builder(code);
    }

    public static Builder message(String message){
        return new Builder(message);
    }

    public static Builder ok(){
        return code(200).message("success");
    }

    public static <T> R<T> ok(T data){
        return ok().data(data);
    }

    public static <T> R<T> error(){
        return error("error");
    }

    public static <T> R<T> error(String message){
        return code(500).message(message).build();
    }
    public static class Builder{
        private int code;
        private String message;

        private Builder(){}
        private Builder(int code){
            this.code = code;
        }
        private Builder(String message){
            this.message = message;
        }

        public Builder code(int code){
            this.code = code;
            return this;
        }

        public Builder message(String message){
            this.message = message;
            return this;
        }

        public <T> R<T> build(){
            return data(null);
        }

        public <T> R<T> data(T data){
            return new R(this.code,this.message,data);
        }
    }
}