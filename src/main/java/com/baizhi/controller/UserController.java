package com.baizhi.controller;


import com.baizhi.entity.User;
import com.baizhi.service.IUserService;
import com.baizhi.vo.R;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author huiJie
 * @since 2021-08-24
 */
@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    private IUserService userService;
    @GetMapping
    public R login(String userName,String passWord){
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        wrapper.eq("user_name",userName).eq("pass_word",passWord);
        User user = userService.getOne(wrapper);
        System.out.println("user = " + user);
        if (user!=null){
            return R.ok(null);
        }else {
            return R.error();
        }
    }
}

