package com.baizhi.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author huiJie
 * @since 2021-08-24
 */
@RestController
@RequestMapping("/student")
public class StudentController {

}

