package com.baizhi.service;

import com.baizhi.entity.Student;
import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author huiJie
 * @since 2021-08-24
 */
public interface IStudentService extends IService<Student> {
    PageInfo<Student> showPage(Integer pageNum,Integer pageSize);
}
