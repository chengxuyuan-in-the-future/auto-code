package com.baizhi.service.impl;

import com.baizhi.entity.Record;
import com.baizhi.mapper.RecordMapper;
import com.baizhi.service.IRecordService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author huiJie
 * @since 2021-08-24
 */
@Service
public class RecordServiceImpl extends ServiceImpl<RecordMapper, Record> implements IRecordService {

    @Override
    @Transactional(readOnly = true)
    public List<Record> showPageRecord(Integer pageNum, Integer pageSize) {
        Page<Record> page = new Page<>(pageNum,pageSize);
        IPage<Record> selectPage = baseMapper.selectPage(page,null);
        return selectPage.getRecords();
    }
}
