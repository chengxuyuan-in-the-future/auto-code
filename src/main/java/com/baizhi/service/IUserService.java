package com.baizhi.service;

import com.baizhi.entity.User;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author huiJie
 * @since 2021-08-24
 */
public interface IUserService extends IService<User> {

}
