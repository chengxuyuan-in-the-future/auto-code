package com.baizhi.service;

import com.baizhi.entity.Record;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author huiJie
 * @since 2021-08-24
 */
public interface IRecordService extends IService<Record> {
    List<Record> showPageRecord(Integer PageNum,Integer pageSize);
}
