package com.baizhi.mapper;

import com.baizhi.entity.Student;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author huiJie
 * @since 2021-08-24
 */
public interface StudentMapper extends BaseMapper<Student> {
    Student showOne(Integer id);
}
