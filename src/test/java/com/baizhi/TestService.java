package com.baizhi;

import com.baizhi.entity.Record;
import com.baizhi.entity.Student;
import com.baizhi.entity.User;
import com.baizhi.service.IRecordService;
import com.baizhi.service.IStudentService;
import com.baizhi.service.IUserService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.github.pagehelper.PageInfo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
public class TestService {
    @Autowired
    private IUserService userService;
    @Autowired
    private IRecordService recordService;
    @Autowired
    private IStudentService studentService;
    @Test
    public void showPage(){
        List<Record> records = recordService.showPageRecord(1, 1);
        records.forEach(System.out::println);
    }
    @Test
    public void showPage1(){
        IPage<Record> page = recordService.page(new Page<>(1, 2));
        List<Record> records = page.getRecords();
        records.forEach(System.out::println);
    }
    @Test
    public void showPageStudent(){
        PageInfo<Student> studentPageInfo = studentService.showPage(1, 2);
        List<Student> list = studentPageInfo.getList();
        list.forEach(System.out::println);
    }
    @Test
    public void testUser(){
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        wrapper.eq("user_name","白冰").eq("pass_word","9999");
        User user = userService.getOne(wrapper);
        System.out.println("user = " + user);
    }
}
