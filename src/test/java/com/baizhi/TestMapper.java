package com.baizhi;

import com.baizhi.entity.User;
import com.baizhi.mapper.RecordMapper;
import com.baizhi.mapper.StudentMapper;
import com.baizhi.entity.Student;
import com.baizhi.mapper.UserMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
public class TestMapper {
    @Resource
    private StudentMapper studentMapper;
    @Resource
    private UserMapper userMapper;
    @Test
    public void add(){
        studentMapper.insert(new Student(null,"文强",1,"计算机一班",1,LocalDate.now(),"15223335656",1,null));
    }
    @Test
    public void show(){
        Student student = studentMapper.showOne(1);
        System.out.println("student = " + student);
    }
    @Test
    public void local(){
        LocalDateTime now = LocalDateTime.now();
        LocalDate now1 = LocalDate.now();
        System.out.println("now1 = " + now1);
        System.out.println("now = " + now);
    }
    @Test
    public void testUser(){
        List<User> users = userMapper.selectList(null);
        users.forEach(System.out::println);
    }
}
